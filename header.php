<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <div class="page-wrapper">
        <a href="#main-content" class="screen-reader">Skip To Main Content</a>
        <header>
            <div class="header-grid">
                <div class="top-bar-logo-wrap">
                    <a title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(network_site_url('/')); ?>" class="health-top-bar-logo-link">
                        <?php
                            if (is_mainsite()) {
                                
                        ?>
                                <img id="top-bar-logo-vertical" class="health-top-bar-logo" src="<?php echo HEALTH_THEME_URL . '/img/uconn-health-logo-vertical.png' ?>" alt="UConn Health">
                                <img id="top-bar-logo-horizontal" class="health-top-bar-logo" src="<?php echo HEALTH_THEME_URL . '/img/uconn-health-logo-horizontal.png' ?>" alt="UConn Health">
                        <?php
                            } else {
                        ?>
                                <img id="top-bar-logo-horizontal" class="health-top-bar-logo" src="<?php echo HEALTH_THEME_URL . '/img/uconn-health-logo-horizontal.png' ?>" alt="UConn Health">
                        <?php
                            }
                        ?>
                    </a>
                </div>
                <div class="top-links-wrap">
                    <div class="top-links-container">
                        <div class="top-bar-links-wrap">
                            <?php
                                if (is_mainsite()) {
                            ?>
                                    <a href="http://s.uconn.edu/uconnhealthgiving" class="health-top-bar-btn">Give</a>
                            <?php
                                }
                            ?>
                            <a href="#" class="health-top-bar-btn">Translate</a>
                            <a href="http://health.uconn.edu/search" class="health-top-bar-btn" title="search"><i class="fas fa-search"></i></a>
                            <a href="http://health.uconn.edu/az/" class="health-top-bar-btn">A-Z</a>

                        </div>
                    </div>
                </div>
                
                <div class="nav-container">
                    <?php
                        wp_nav_menu(array(
                            'theme_location' => 'header',
                            'items_wrap' => '<ul id="%1$s" class="am-click-menu %2$s">%3$s</ul>',
                            'menu_id' => 'am-main-menu',
                            'container' => 'nav',
                            'container_id' => 'am-navigation',
                            'walker' => new A11y\Menu_Walker()
                        ));
                    ?>
                </div>
            </div>
        </header>