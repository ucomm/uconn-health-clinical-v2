<?php get_header(); ?>

    <main>
        <section>

            <?php
                if (!UComm\WPThemeBeaverBuilderHelpers::is_builder_enabled()) {
                    get_template_part('template-parts/content', 'loop');
                } else {
                    get_template_part('template-parts/content', 'loop-bb');
                }



            ?>
        </section>
    </main>

<?php get_footer(); ?>