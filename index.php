<?php get_header(); ?>

    <main>
        <section>
            <h1><?php esc_html_e( 'Index', 'theme-boilerplate' ); ?></h1>
            <?php get_template_part('template-parts/content', 'loop'); ?>
        </section>
    </main>

<?php get_footer(); ?>