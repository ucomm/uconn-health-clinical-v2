/**
 * 
 * This part of the webpack config should be used in all environments
 * 
 */

const path = require('path')

module.exports = {
  entry: path.resolve(__dirname, 'src', 'js', 'index.js'),
  // output: {
  //   filename: '[name].[contenthash].js',
  //   path: path.resolve(__dirname, 'build', 'js')
  // },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
}