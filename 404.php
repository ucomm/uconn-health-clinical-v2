<?php get_header(); ?>

    <main role="main" aria-label="Content">
        <section>
            <article id="post-404">
                <h1><?php esc_html_e( 'Page not Found', 'theme-boilerplate' ); ?></h1>

                <p><a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return to the homepage.', 'theme-boilerplate' ); ?></a><p>
            </article>
        </section>
    </main>

<?php get_footer(); ?>