<?php

namespace UConnHealth\Lib;

class Customizer {

  public static function register_theme_customization($wp_customize) {

    $wp_customize->add_section('uconn-health', array(
      'title' => 'UConn Health',
      'priority' => 10,
      'description' => __('Customize the UConn Health Theme', 'uconn-health-clinical')
    ));

    $wp_customize->add_setting('main_site', array(
      'default' => 0,
      'sanitize_callback' => 'esc_attr'
    ));

    $wp_customize->add_control(new \WP_Customize_Control($wp_customize, 'main_site', array(
      'label' => __('UConn Health Main Site?', 'uconn-health-clinical'),
      'section' => 'uconn-health',
      'settings' => 'main_site',
      'type' => 'checkbox'
    )));    
  }

}