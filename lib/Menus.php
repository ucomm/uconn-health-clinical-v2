<?php

namespace UConnHealth\Lib;

class Menus {

  static public function register_menus() {
    $theme_menus = array(
      'footer' => 'Footer Menu',
      'header' => 'Main Menu',
      'primary_footer' => 'Primary Footer Menu'
    );
    foreach ($theme_menus as $slug => $name) {
      register_nav_menu($slug, __($name), 'uconn-health-clinical');
    }
  }

}