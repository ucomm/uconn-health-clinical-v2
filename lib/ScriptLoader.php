<?php

namespace UConnHealth\Lib;

class ScriptLoader {

    private static function get_scripts() {
        $js_dir = glob(HEALTH_THEME_DIR . '/build/js/*.js');
        
        foreach ($js_dir as $file) {

            if (pathinfo($file, PATHINFO_EXTENSION) === 'js') {
                $full_name = basename($file);

                $name = substr(basename($full_name), 0, strpos(basename($full_name), '.'));

                switch ($name) {
                    case 'main':
                        $deps = array('jquery', 'a11y-menu');
                        break;
                    
                    default:
                        $deps = null;
                        break;
                }
            }
            wp_enqueue_script($name, HEALTH_THEME_URL . '/build/js/' . $full_name, $deps, null, true);
        }
    }

    private static function get_styles() {
        $css_dir = glob(HEALTH_THEME_DIR . '/build/css/*.css');
        
        foreach ($css_dir as $file) {

            if (pathinfo($file, PATHINFO_EXTENSION) === 'css') {
                $full_name = basename($file);

                $name = substr(basename($full_name), 0, strpos(basename($full_name), '.'));

                switch ($name) {
                    case 'main':
                        $deps = array('fontawesome', 'a11y-menu');
                        break;
                    
                    default:
                        $deps = null;
                        break;
                }
            }
            wp_enqueue_style($name, HEALTH_THEME_URL . '/build/css/' . $full_name, $deps);
        }
    }

    public static function load_scripts() {
        self::get_styles();
        self::get_scripts();
        wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css');
        wp_enqueue_style('a11y-menu', HEALTH_THEME_URL . '/vendor/ucomm/a11y-menu/dist/main.css');

        wp_enqueue_script('a11y-menu', HEALTH_THEME_URL . '/vendor/ucomm/a11y-menu/dist/Navigation.js', array(), false, true);
    }

}