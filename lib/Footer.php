<?php

namespace UConnHealth\Lib;

class Footer {
  public static function display_bottom_menu() {
    $theme_location = 'footer';
    $args = array(
      'theme_location' => $theme_location,
      'menu_id' => 'lower-footer-menu',
      'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    );
    if (has_nav_menu($theme_location)) {
      wp_nav_menu($args);
    }
  }
}