<?php

namespace UConnHealth\Lib;

class Helpers {

  /**
   * Adds an extra class to the body of the document.
   * helps distinguish between main and clincal sites
   *
   * @param array classes applied to the body of the document
   * @return array
   */
  static public function add_site_type_class($classes) {
    $type = is_mainsite() ? 'main-site' : 'clinical-site';
    $classes['site-type'] = $type;
    return $classes;
  }

  /**
   * A wrapper to manage widget areas.
   *
   * @return void
   */
  static public function register_sidebar_widgets() {
    $widgets = array(
      array(
        'name' => __('Footer Widget 1', 'uconn-health-clinical'),
        'id' => 'footer_widget_1',
        'before_title' => '<p class="widgettitle><span>"',
        'after_title' => '</span></p>',
        'before_widget' => '<div class="widget footer-widget"><div>',
        'after_widget' => '</div></div>'
      ),
      array(
        'name' => __('Footer Widget 2', 'uconn-health-clinical'),
        'id' => 'footer_widget_2',
        'before_title' => '<p class="widgettitle"><span>',
        'after_title' => '</span></p>',
        'before_widget' => '<div class="widget footer-widget"><div>',
        'after_widget' => '</div></div>'
      ),
      array(
        'name' => __('Footer Widget 3', 'uconn-health-clinical'),
        'id' => 'footer_widget_3',
        'before_title' => '<p class="widgettitle><span>"',
        'after_title' => '</span></p>',
        'before_widget' => '<div class="widget footer-widget"><div>',
        'after_widget' => '</div></div>'
      ),
      array(
        'name' => __('Footer Widget 4', 'uconn-health-clinical'),
        'id' => 'footer_widget_4',
        'before_title' => '<p class="widgettitle><span>"',
        'after_title' => '</span></p>',
        'before_widget' => '<div class="widget footer-widget"><div>',
        'after_widget' => '</div></div>'
      )
    );
    \UComm\WPThemeHelpers::register_sidebar_widgets($widgets);
  }

}