const navigation = new Navigation({ click: true })

document.addEventListener('DOMContentLoaded', () => {

  navigation.init()

});