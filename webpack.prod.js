const path = require('path')
const merge = require('webpack-merge')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'build', 'js')
  },
  mode: 'production',
})
