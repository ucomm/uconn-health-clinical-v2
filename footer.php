

        <footer>
            <section class="footer-widget-wrap">
                <div class="footer-widget-container">
                    <?php
                        for ($i = 1; $i < 5; $i++) {
                            $widget = 'footer_widget_' . $i;
                            if (is_active_sidebar($widget)) {
                                dynamic_sidebar($widget);
                            }
                        }
                    ?>
                </div>
            </section>
            <section class="footer-widget-bottom-wrap">
                <div class="footer-widget-bottom-container">
                    <?php UConnHealth\Lib\Footer::display_bottom_menu(); ?>
                </div>
            </section>
        </footer>
    </div><!-- /wrapper -->

    <?php wp_footer(); ?>

</body>
</html>