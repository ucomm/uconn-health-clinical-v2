<!-- search -->
<form class="search" method="get" action="<?php echo esc_url( home_url() ); ?>">
    <div role="search">
        <label for="site-search">Search</label>
        <input id="site-search" class="search-input" type="search" name="s" aria-label="Search site for:" placeholder="<?php esc_html_e( 'To search, type and hit enter.', 'theme-boilerplate' ); ?>">
        <button class="search-submit" type="submit"><?php esc_html_e( 'Search', 'theme-boilerplate' ); ?></button>
    </div>
</form>
<!-- /search -->