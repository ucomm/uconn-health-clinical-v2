<?php

// Constants
define( 'HEALTH_THEME_DIR', get_stylesheet_directory() );
define( 'HEALTH_THEME_URL', get_stylesheet_directory_uri() );

// Load
// select the right composer autoload.php file depending on environment.
if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
	require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
	require_once(ABSPATH . 'vendor/autoload.php');
} else {
	require_once('vendor/autoload.php');
}

function is_mainsite() {
	return get_theme_mod('main_site', "0") === "1" ? true : false;
}


// require files from lib
require_once( 'lib/Customizer.php' );
require_once( 'lib/Footer.php' );
require_once( 'lib/Helpers.php' );
require_once( 'lib/Menus.php' );
require_once( 'lib/ScriptLoader.php' );

// add custom fonts to beaver builder
$custom_fonts = array(
	'Proxima Nova' => array(
		'fallback' => 'Verdana, Arial, Helvetica, sans-serif',
		'weights' => array(
			'300',
			'400',
			'600',
			'700'
		)
	)
);

UComm\WPThemeBeaverBuilderHelpers::add_fonts_to_beaver_builder($custom_fonts);

// register nav menus
add_action('after_setup_theme', array('UConnHealth\Lib\Menus', 'register_menus'));

// register sidebar widgets
add_action('widgets_init', array('UConnHealth\Lib\Helpers', 'register_sidebar_widgets'));

// Add mime types
add_action('upload_mimes', array('UComm\WPThemeFileTypes','upload_mime_types'));

// Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', array('UConnHealth\Lib\ScriptLoader', 'load_scripts') );

// register customizer areas
add_action('customize_register', array('UConnHealth\Lib\Customizer', 'register_theme_customization'));

// filter body class
add_filter('body_class', array('UConnHealth\Lib\Helpers', 'add_site_type_class'));