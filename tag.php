<?php get_header(); ?>

    <main role="main" aria-label="Content">
        <section>
            <h1><?php esc_html_e( 'Tag: ', 'theme-boilerplate' ); single_tag_title( '', false ); ?></h1>
            <?php get_template_part('template-parts/content', 'loop'); ?>
        </section>
    </main>

<?php get_footer(); ?>